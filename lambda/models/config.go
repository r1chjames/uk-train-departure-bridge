package models

type AppConfig struct {
	DepartureStation   string
	DestinationStation string
	FutureTrainCount   int
	IotTopic           string
	Username           string
	Password           string
}
