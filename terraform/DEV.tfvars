env                      = "DEV"
departure_station        = "WDS"
destination_station      = "LDS"
future_train_count       = 3
departures_topic         = "departures/train/new"
commands_topic           = "departures/train/commands"
refresh_interval_in_mins = "5 minutes"

